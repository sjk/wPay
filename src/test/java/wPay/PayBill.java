package wPay;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import com.jfinal.weixin.sdk.api.PaymentApi;

public class PayBill {

	public static void main(String[] args) {
		/**
		<xml>
		  <appid>wx2421b1c4370ec43b</appid>
		  <bill_date>20141110</bill_date>
		  <bill_type>ALL</bill_type>
		  <mch_id>10000100</mch_id>
		  <nonce_str>21df7dc9cd8616b56919f20d9f679233</nonce_str>
		  <sign>332F17B766FC787203EBE9D6E40457A1</sign>
		</xml>
		*/
		String appid = "wxea6c716c929092e7";
		String mch_id = "1466755602";
		String paternerKey = "08GBiFrXrMENPX18hjN6cSUvyRHfwo8r";
		String billDate="20171012";
//		long  start = System.currentTimeMillis();
		String resContent = PaymentApi.downloadBill(appid, mch_id, paternerKey, billDate);
//		Map<String, String> xmlToMap = PaymentKit.xmlToMap(resContent);
//		for (String string : xmlToMap.keySet()) {
////			System.out.println(xmlToMap.get(string));
//		}
//		System.out.println(resContent);
		String [] resContentStr = resContent.split("\\r?\\n"); 
		 int length = resContentStr.length;
		 System.out.println(length);
		 if(length<4) {
			 return;
		 }
//		 length = length-2;
		 for (int i=0;i<length;i++) {
			 System.out.println(resContentStr[i]);
//			 String[] split = resContentStr[i].split(",");
//			 for (String string : split) {
//				System.out.println(string.substring(1));
//			}
		}
//		long end = System.currentTimeMillis();
//		Map<String, String> queryByTransactionId = PaymentApi.queryByTransactionId(appid, mch_id, paternerKey, "4200000015201710229634371392");
//		for (String string : queryByTransactionId.keySet()) {
//			System.out.println(queryByTransactionId.get(string));
//		}
//		Double d = Double.parseDouble(("0.04"));
//		d = d*100;
//		System.out.println(d.longValue());
	}
	
	public static byte[] uncompress(byte[] bytes) {  
		byte[] msg = null;
        if (bytes == null || bytes.length == 0) {  
            return null;  
        }  
        try {
        	ByteArrayOutputStream out = new ByteArrayOutputStream();  
            try {
            	ByteArrayInputStream in = new ByteArrayInputStream(bytes);  
            	try {
            		 GZIPInputStream ungzip = new GZIPInputStream(in);  
            		 try {
            			 byte[] buffer = new byte[256];  
                         int n;  
                         while ((n = ungzip.read(buffer)) >= 0) {  
                             out.write(buffer, 0, n);  
                         }  
                         out.flush();
                         msg = out.toByteArray();
            		 }finally {
            			 ungzip.close();
            		 }
                     
            	}finally {
            		in.close();
            	}
            }finally {
            	out.close();
            }
        }catch (IOException e) {
			e.printStackTrace();
		}
        return msg;
    }  
}
