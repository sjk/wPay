/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.api;

import com.jfinal.weixin.sdk.utils.HttpUtils;

/**
 * 模板消息 API
 * 文档地址：http://mp.weixin.qq.com/wiki/17/304c1885ea66dbedf7dc170d84999a9d.html
 */
public class TemplateMsgApi {
	
	private static String sendApiUrl = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=";
	private static String apiAddTemplate = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token=";
	private static String del_private_template = "https://api.weixin.qq.com/cgi-bin/template/del_private_template?access_token=";
	private static String get_all_private_template = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token=";
	private static String get_industry = "https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=";
	private static String api_set_industry = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=";
	
	/**
	 * 发送模板消息
	 * @param jsonStr json字符串
	 * @return {ApiResult}
	 */
	public static ApiResult send(String jsonStr) {
		String jsonResult = HttpUtils.post(sendApiUrl + AccessTokenApi.getAccessToken().getAccessToken(), jsonStr);
		return new ApiResult(jsonResult);
	}
	public static ApiResult getTemplateId(String jsonStr) {
		String jsonResult = HttpUtils.post(apiAddTemplate + AccessTokenApi.getAccessToken().getAccessToken(), jsonStr);
		return new ApiResult(jsonResult);
	}
	public static ApiResult deleteTemplate(String jsonStr) {
		String jsonResult = HttpUtils.post(del_private_template + AccessTokenApi.getAccessToken().getAccessToken(), jsonStr);
		return new ApiResult(jsonResult);
	}
	public static ApiResult getAllTemplates() {
		String jsonResult = HttpUtils.get(get_all_private_template+AccessTokenApi.getAccessToken().getAccessToken());
		return new ApiResult(jsonResult);
	}
	public static ApiResult getApiSetIndustry() {
		String jsonResult = HttpUtils.get(get_industry+AccessTokenApi.getAccessToken().getAccessToken());
		return new ApiResult(jsonResult);
	}
	
	public static ApiResult setApiIndustry(String jsonStr) {
		String jsonResult = HttpUtils.post(api_set_industry + AccessTokenApi.getAccessToken().getAccessToken(), jsonStr);
		return new ApiResult(jsonResult);
	}
	
}


