package com.jfinal.weixin.sdk.cache;

import com.cdqidi.util.RedisMgr;

public class RedisCache implements IAccessTokenCache{
	private static final String prefix="wx_accesstoken_";
	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(String key) {
		Object value=null;
		try {
			value = RedisMgr.getInstance().get(prefix+key);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return (T) value;
	}

	@Override
	public void set(String key, Object value) {
		try {
			RedisMgr.getInstance().set(prefix+key, value);
			RedisMgr.getInstance().expire(prefix+key, DEFAULT_TIME_OUT-55);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void remove(String key) {
		try {
			RedisMgr.getInstance().del(prefix+key);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}