package com.cdqidi.entity;

import java.util.Date;

public class PayOrder {
	private String orderId;//商户订单号
	private String mchId;//商户号
	private String personid;//付款人
	private String openid;//付款公众号标示
	private String mobile;//手机号
	private Integer orderDays;//订单时长
	private Long payAmount;//支付金额
	private String ipAddress;//终端ip
	private Date timeStart;//支付开始时间
	private String payCode;//支付状态
	private String wxorgid;//公众号
	private String orgid;//学校
	private String pwxorgid;//支付公众号
	private String stuid;//学生id
	private String chargecode;//套餐编码
	private String delChargeCode;//删除套餐编码
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getMchId() {
		return mchId;
	}
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	public String getPersonid() {
		return personid;
	}
	public void setPersonid(String personid) {
		this.personid = personid;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Integer getOrderDays() {
		return orderDays;
	}
	public void setOrderDays(Integer orderDays) {
		this.orderDays = orderDays;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Date getTimeStart() {
		return timeStart;
	}
	public void setTimeStart(Date timeStart) {
		this.timeStart = timeStart;
	}
	public String getPayCode() {
		return payCode;
	}
	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}
	public String getWxorgid() {
		return wxorgid;
	}
	public void setWxorgid(String wxorgid) {
		this.wxorgid = wxorgid;
	}
	public String getOrgid() {
		return orgid;
	}
	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}
	public String getPwxorgid() {
		return pwxorgid;
	}
	public void setPwxorgid(String pwxorgid) {
		this.pwxorgid = pwxorgid;
	}
	public String getStuid() {
		return stuid;
	}
	public void setStuid(String stuid) {
		this.stuid = stuid;
	}
	public String getChargecode() {
		return chargecode;
	}
	public void setChargecode(String chargecode) {
		this.chargecode = chargecode;
	}
	public Long getPayAmount() {
		return payAmount;
	}
	public void setPayAmount(Long payAmount) {
		this.payAmount = payAmount;
	}
	public String getDelChargeCode() {
		return delChargeCode;
	}
	public void setDelChargeCode(String delChargeCode) {
		this.delChargeCode = delChargeCode;
	}
}