package com.cdqidi.entity;

/**
 * 企业付款到个人
 * 
 * @author sjkyll
 *
 */
public class CorpPay {
	private String pwxorgid;// 支付公众号wxorgid
	private String popenid;// 支付公众中当前人的openid
	private Integer amount;// 金额
	private String mobile;// 手机号
	private String orderid;// 微信订单号，唯一，32位
	private String checkName;//// NO_CHECK：不校验真实姓名 FORCE_CHECK：强校验真实姓名
	private String companyDesc;// 企业介绍
	private String appid;//公众号id
	private String mchid;//商户号
	private String sign;//签名
	private String nonceStr;//随机字符串
	private String apiKey;
	
	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getMchid() {
		return mchid;
	}

	public void setMchid(String mchid) {
		this.mchid = mchid;
	}

	public String getPwxorgid() {
		return pwxorgid;
	}

	public void setPwxorgid(String pwxorgid) {
		this.pwxorgid = pwxorgid;
	}

	public String getPopenid() {
		return popenid;
	}

	public void setPopenid(String popenid) {
		this.popenid = popenid;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getCheckName() {
		return checkName;
	}

	public void setCheckName(String checkName) {
		this.checkName = checkName;
	}

	public String getCompanyDesc() {
		return companyDesc;
	}

	public void setCompanyDesc(String companyDesc) {
		this.companyDesc = companyDesc;
	}

	public void freeData() {
		this.pwxorgid = null;
		this.popenid = null;
		this.amount = null;
		this.mobile = null;
		this.orderid = null;
		this.checkName = null;
		this.companyDesc = null;
	}
}
