package com.cdqidi.entity;

public class User {

	private String schoolid;//学生学校
	private String schoolName;
	private String parid;//家长id
	private String parName;//家长姓名
	private String stuid;//学生id
	private String stuName;//学生姓名
	private String wxorgid;//公众号id
	private String payWxorgid;//支付公众号id
	private String payOpenid;//支付公众号的openid
	private String openid;//家长openid
	private String mobile;//家长手机号码
	private String url;//支付成功以后需要跳转的url地址
	
	private String delChargeCode;//需要删除的套餐编号，一般为体验套餐
	
	
	private String chargeCode;//套餐
	private String tclx;//套餐类型
	private String chargeInfo;//套餐说明
	private Integer month;//订购数量（按月订购）
	private Integer amount;//订单总金额
	private Integer days;//按天订购
	
	//支付相关
	private String appId;
	private String apiKey;
	private String mchId;
	
	public String getDelChargeCode() {
		return delChargeCode;
	}
	public void setDelChargeCode(String delChargeCode) {
		this.delChargeCode = delChargeCode;
	}
	public String getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(String schoolid) {
		this.schoolid = schoolid;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getParid() {
		return parid;
	}
	public void setParid(String parid) {
		this.parid = parid;
	}
	public String getParName() {
		return parName;
	}
	public void setParName(String parName) {
		this.parName = parName;
	}
	public String getStuid() {
		return stuid;
	}
	public void setStuid(String stuid) {
		this.stuid = stuid;
	}
	public String getStuName() {
		return stuName;
	}
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}
	public String getWxorgid() {
		return wxorgid;
	}
	public void setWxorgid(String wxorgid) {
		this.wxorgid = wxorgid;
	}
	public String getPayWxorgid() {
		return payWxorgid;
	}
	public void setPayWxorgid(String payWxorgid) {
		this.payWxorgid = payWxorgid;
	}
	public String getPayOpenid() {
		return payOpenid;
	}
	public void setPayOpenid(String payOpenid) {
		this.payOpenid = payOpenid;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getChargeCode() {
		return chargeCode;
	}
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
	public String getTclx() {
		return tclx;
	}
	public void setTclx(String tclx) {
		this.tclx = tclx;
	}
	public String getChargeInfo() {
		return chargeInfo;
	}
	public void setChargeInfo(String chargeInfo) {
		this.chargeInfo = chargeInfo;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getMchId() {
		return mchId;
	}
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getDays() {
		return days;
	}
	public void setDays(Integer days) {
		this.days = days;
	}
}