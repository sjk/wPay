package com.cdqidi.entity;
public class SchoolCharge {

	private String chargecode;
	private String chargename;
	private Double chargeprice;
	private String info;
	public String getChargecode() {
		return chargecode;
	}
	public void setChargecode(String chargecode) {
		this.chargecode = chargecode;
	}
	public String getChargename() {
		return chargename;
	}
	public void setChargename(String chargename) {
		this.chargename = chargename;
	}
	public Double getChargeprice() {
		return chargeprice;
	}
	public void setChargeprice(Double chargeprice) {
		this.chargeprice = chargeprice;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
}