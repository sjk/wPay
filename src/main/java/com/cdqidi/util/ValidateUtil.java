package com.cdqidi.util;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class ValidateUtil {

	
	public static boolean  isMobile(String mobile){
		Pattern p = Pattern.compile("^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1})|(14[0-9]{1}))+\\d{8})$");
		Matcher m = p.matcher(mobile);
		return m.matches();
	}
	public static void main(String[] args) {
		System.out.println(isMobile("18608163868"));
	}
	
	public static void paramIsNull(HashMap<String, Object> map,String ... str) {
		boolean b=false;
		for(String s:str) {
			if(StringUtils.isEmpty(s)) {
				b = true;
				break;
			}
		}
		if(b) {
			map.put("err_code", 1000);
			map.put("err_msg", "参数不能为空");
		}
	}
}
