
package com.cdqidi.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.Set;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisMgr {
	
	private RedisMgr(){
		this.startup();
	}
	private static class SingletonHolder {
		private static RedisMgr INSTANCE = new RedisMgr();
	}

	public static RedisMgr getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	private JedisPool	pool;


	public void startup() {
		pool = new JedisPool(new JedisPoolConfig(),StaticResource.HOST, StaticResource.PORT, StaticResource.TIMEOUT);
	}

	public void shutdown() {
		pool.destroy();
	}

	public Long expire(String id, int sec) throws IOException {
		Jedis jedis = pool.getResource();
		try {
			return jedis.expire(id.getBytes(), sec);
		} finally {
			jedis.close();
		}
	}

	public Long expireProp(String id, int sec) throws IOException {
		Jedis jedis = pool.getResource();
		try {
			return jedis.expire(id, sec);
		} finally {
			jedis.close();
		}
	}

	public Long del(String id) throws IOException {
		Jedis jedis = pool.getResource();
		try {
			return jedis.del(id.getBytes());
		} finally {
			jedis.close();
		}
	}

	public Long delProp(String id) throws IOException {
		Jedis jedis = pool.getResource();
		try {
			return jedis.del(id);
		} finally {
			jedis.close();
		}
	}

	public String setProp(String id, String obj) throws IOException {
		Jedis jedis = pool.getResource();
		try {
			return jedis.set(id, obj);
		} finally {
			jedis.close();
		}
	}

	public String getProp(String id) throws IOException, ClassNotFoundException {
		Jedis jedis = pool.getResource();
		try {
			return jedis.get(id);
		} finally {
			jedis.close();
		}
	}

	public String set(String id, Object obj) throws IOException {
		Jedis jedis = pool.getResource();
		try {
			return jedis.set(id.getBytes(), writeObj(obj));
		} finally {
			jedis.close();
		}
	}

	public Object get(String id) throws IOException, ClassNotFoundException {
		Jedis jedis = pool.getResource();
		try {
			byte[] bs = jedis.get(id.getBytes());
			if (bs != null) {
				return readObj(bs);
			}
			return null;
		} finally {
			jedis.close();
		}
	}

	public Set<String> getKeys(String pattern){
		Jedis jedis = pool.getResource();
		try{
			return jedis.keys(pattern);
		}finally{
			jedis.close();
		}
	}
	
	public Long delAll(Set<String> ids) throws IOException {
		Jedis jedis = pool.getResource();
		try {
			byte[][] b = new byte[ids.size()][];
			if(null!=ids){
				Iterator<String> iterator = ids.iterator();
				String id = null;
				int i=0;
				while(iterator.hasNext()){
					id = iterator.next();
					b[i] = id.getBytes();
					i++;
				}
			}
			return jedis.del(b);
		} finally {
			jedis.close();
		}
	}
	
	public Long delPropAll(Set<String> ids) throws IOException {
		Jedis jedis = pool.getResource();
		try {
			String[] _ids = new String[ids.size()];
			if(null!=ids){
				Iterator<String> iterator = ids.iterator();
				int i=0;
				while(iterator.hasNext()){
					_ids[i] = iterator.next();
					i++;
				}
			}
			return jedis.del(_ids);
		} finally {
			jedis.close();
		}
	}
	
	private Object readObj(byte[] data) throws IOException, ClassNotFoundException {
		ByteArrayInputStream byte_in = new ByteArrayInputStream(data, 0, data.length);
		try {
			ObjectInputStream myreader = new ObjectInputStream(byte_in);
			try {
				return myreader.readObject();
			} finally {
				myreader.close();
			}
		} finally {
			byte_in.close();
		}
	}

	private byte[] writeObj(Object obj) throws IOException {
		ByteArrayOutputStream byte_out = new ByteArrayOutputStream();
		try {
			ObjectOutputStream mywriter = new ObjectOutputStream(byte_out);
			try {
				mywriter.writeObject(obj);
				return byte_out.toByteArray();
			} finally {
				mywriter.close();
			}
		} finally {
			byte_out.close();
		}
	}
}
