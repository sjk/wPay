package com.cdqidi.util;

public class RedisId {
	private RedisId(){
		
	}
	private static class SingletonHolder {
		private static RedisId INSTANCE = new RedisId();
	}

	public static RedisId getInstance() {
		return SingletonHolder.INSTANCE;
	}
	public static final int TIME = 2 * 24 * 60 * 60;
	
	
	/**
	 * OrgWxconfig
	 * @param wxorgid
	 * @return
	 */
	public String createWxConfig(String wxorgid){
		StringBuilder str = new StringBuilder();
		str.append("wxconfig_");
		str.append(wxorgid);
		return str.toString().intern();
	}
}