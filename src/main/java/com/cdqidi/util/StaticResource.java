package com.cdqidi.util;

import com.jfinal.kit.PropKit;

public class StaticResource {
	
	public static final String HOST;
	public static final Integer PORT;
	public static final Integer TIMEOUT;

	public static final String URL;
	public static final String CID;
	
	public static final String CERTPATH;//商户证书存放路径
	public static final String SERVERIP;//支付所在机器的ip地址
	
	
	static{
		HOST = PropKit.use("/res/weixin_config.txt").get("host");
		PORT = PropKit.use("/res/weixin_config.txt").getInt("port");
	    TIMEOUT = PropKit.use("/res/weixin_config.txt").getInt("timeout");
	    URL = PropKit.use("/res/weixin_config.txt").get("url");
	    CID = PropKit.use("/res/weixin_config.txt").get("cid");
	    CERTPATH = PropKit.use("/res/weixin_config.txt").get("certPath");
	    SERVERIP = PropKit.use("/res/weixin_config.txt").get("serverIp");
	}
}
