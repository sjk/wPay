package com.cdqidi.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.cdqidi.util.IPUtil;
import com.cdqidi.util.IpAuth;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.weixin.sdk.kit.IpKit;

public class IpAuthInterceptor implements Interceptor{
	@Override
	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		String realIp = IpKit.getRealIp(controller.getRequest());
		if (StrKit.isBlank(realIp)) {
			realIp = "127.0.0.1";
		}
		try {
			realIp = IPUtil.hexToIP(IPUtil.ipToHex(realIp));
	    }catch (Exception ex){
	    	realIp = "127.0.0.1";
	    }
		String cid = controller.getPara("cid");
		JSONObject jo = new JSONObject(true);
		IpAuth.yanzehen(cid, realIp, jo);
		if(jo.size()>0){
			controller.renderJson(jo.toJSONString());
			return;
		}
		inv.invoke();
	}
}
