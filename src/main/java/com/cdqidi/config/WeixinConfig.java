package com.cdqidi.config;

import org.apache.log4j.Logger;

import com.cdqidi.controller.AuthController;
import com.cdqidi.controller.WeixinPayController;
import com.cdqidi.model.OrgWxconfig;
import com.cdqidi.util.RedisMgr;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.json.FastJsonFactory;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log4jLogFactory;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.render.ViewType;
import com.jfinal.weixin.sdk.api.ApiConfigKit;
import com.jfinal.weixin.sdk.cache.RedisCache;


/**
 * 对整个 web项目进行配置
 * @author sjk
 *
 */
public class WeixinConfig extends JFinalConfig {
	private static Logger logger = Logger.getLogger(JFinalConfig.class);
	@Override
	public void configConstant(Constants me) {
		
		// 加载少量必要配置，随后可用PropKit.get(...)获取值
		PropKit.use("/res/db_config.txt");
		
		//开发模式，JFinal 会对每次请求输出报告，如输出本次请求的 Controller、Method以及请求所携带的参数
		me.setDevMode(PropKit.getBoolean("devMode", true));
		
		//JFinal 支持 JSP、FreeMarker、Velocity 三种常用视图
		me.setViewType(ViewType.JSP);
		
		// ApiConfigKit 设为开发模式可以在开发阶段输出请求交互的 xml 与 json 数据
		ApiConfigKit.setDevMode(me.getDevMode());
		
		Log4jLogFactory iLogFactory = new Log4jLogFactory();
		me.setLogFactory(iLogFactory);
		
		me.setError500View("/WEB-INF/page/error/500.jsp");
		me.setError404View("/WEB-INF/page/error/404.jsp");
		me.setError401View("/WEB-INF/page/error/401.jsp");
		me.setError403View("/WEB-INF/page/error/403.jsp");
		
		// 默认使用的jackson，下面示例是切换到fastJson
		me.setJsonFactory(new FastJsonFactory());
	}

	@Override
	public void configHandler(Handlers me) {

	}

	@Override
	public void configInterceptor(Interceptors me) {
	}

	@Override
	public void configPlugin(Plugins me) {
		//配置druid连接池
		DruidPlugin dp = new DruidPlugin(PropKit.get("url"), PropKit.get("username"), PropKit.get("password"),PropKit.get("driverClass"), PropKit.get("filters"));
		//初始化连接数量
		dp.setInitialSize(PropKit.getInt("initialSize"));
		//最大并发连接数 
		dp.setMaxActive(PropKit.getInt("maxActive"));
		//配置获取连接等待超时的时间
		dp.setMaxWait(PropKit.getLong("maxWait"));
		//超过时间限制是否回收
		dp.setRemoveAbandoned(PropKit.getBoolean("removeAbandoned"));
		//超过时间限制多长
		dp.setRemoveAbandonedTimeoutMillis(PropKit.getLong("removeAbandonedTimeoutMillis"));
		//配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
		dp.setTimeBetweenEvictionRunsMillis(PropKit.getLong("timeBetweenEvictionRunsMillis"));
		//配置一个连接在池中最小生存的时间，单位是毫秒
		dp.setMinEvictableIdleTimeMillis(PropKit.getLong("minEvictableIdleTimeMillis"));
		//用来检测连接是否有效的sql，要求是一个查询语句
		dp.setValidationQuery(PropKit.get("validationQuery"));
		//申请连接的时候检测
		dp.setTestWhileIdle(PropKit.getBoolean("testWhileIdle"));
		//申请连接时执行validationQuery检测连接是否有效，配置为true会降低性能
		dp.setTestOnBorrow(PropKit.getBoolean("testOnBorrow"));
	    //归还连接时执行validationQuery检测连接是否有效，配置为true会降低性能
		dp.setTestOnReturn(PropKit.getBoolean("testOnReturn"));
	     //打开PSCache，并且指定每个连接上PSCache的大小,是否缓存preparedStatement,mysql5.5以上支持
		dp.setMaxPoolPreparedStatementPerConnectionSize(PropKit.getInt("maxPoolPreparedStatementPerConnectionSize"));
		me.add(dp);
		
		
		//配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
		me.add(arp);
		
		arp.addMapping("org_wxconfig", "id",OrgWxconfig.class);
		
		
		arp.setShowSql(PropKit.getBoolean("devMode", false));
		arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
		 
		//redis cache
		ApiConfigKit.setAccessTokenCache(new RedisCache());
		 
	}

	@Override
	public void configRoute(Routes me) {
		me.add("/pay", WeixinPayController.class);
		me.add("/auth", AuthController.class);
	}
	
	@Override
	public void beforeJFinalStop() {
		RedisMgr.getInstance().shutdown();
	}
	
	@Override
	public void afterJFinalStart() {
		RedisMgr.getInstance().startup();
		logger.info("启动成功");
	}
}