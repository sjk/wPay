package com.cdqidi.service;

import com.cdqidi.model.OrgWxconfig;
import com.jfinal.weixin.sdk.api.ApiConfig;

public class ApiConfigService {

	private ApiConfigService(){
		
	}
	
	private static class SingletonHolder {
		private static ApiConfigService INSTANCE = new ApiConfigService();
	}
	
	public static ApiConfigService getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	public ApiConfig getApiConfig(String wxorgid){
		ApiConfig api = null;
		OrgWxconfig wxConfig = OrgWxConfigService.getInstance().getWxConfig(wxorgid);
		if (null != wxConfig) {
			api = setApiConfig(wxConfig);
		}
		return api;
	}

	public ApiConfig setApiConfig(OrgWxconfig wxConfig) {
		ApiConfig ac = new ApiConfig();
		ac.setToken(wxConfig.getToken());
		ac.setAppId(wxConfig.getAppId());
		ac.setAppSecret(wxConfig.getAppSecret());
		boolean encryptMessage = wxConfig.getEncryptMessage();
		ac.setEncryptMessage(encryptMessage);
		ac.setEncodingAesKey(wxConfig.getEncodingAesKey());
		return ac;
	}
	
}
