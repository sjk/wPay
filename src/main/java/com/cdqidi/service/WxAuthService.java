package com.cdqidi.service;

import com.jfinal.core.Controller;
import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.weixin.sdk.api.SnsAccessToken;
import com.jfinal.weixin.sdk.api.SnsAccessTokenApi;

public class WxAuthService {

	private WxAuthService(){
		
	}
	private static class SingletonHolder {
		private static WxAuthService INSTANCE = new WxAuthService();
	}
	
	public static WxAuthService getInstance() {
		return SingletonHolder.INSTANCE;
	}
	public String getAuthorizeURL(String wxorgid,String url){
		ApiConfig apiConfig = ApiConfigService.getInstance().getApiConfig(wxorgid);
		if(null==apiConfig){
			return null;
		}
		return SnsAccessTokenApi.getAuthorizeURL(apiConfig.getAppId(), url, false);
	}
	public SnsAccessToken getOpenid(String code, String wxorgid) {
		ApiConfig apiConfig = ApiConfigService.getInstance().getApiConfig(wxorgid);
		String appId = apiConfig.getAppId();
		String secret = apiConfig.getAppSecret();
		SnsAccessToken sns = SnsAccessTokenApi.getSnsAccessToken(appId, secret, code);
		return sns;
	}
	
	public void render500(Controller controller,String errorMessage){
		if(null==errorMessage){
			errorMessage="";
		}
		controller.setAttr("errmsg", errorMessage);
		controller.render("/WEB-INF/page/error/500.jsp");
		return;
	}
}
