package com.cdqidi.service;

import java.io.IOException;

import com.alibaba.fastjson.JSON;
import com.cdqidi.model.OrgWxconfig;
import com.cdqidi.util.RedisId;
import com.cdqidi.util.RedisMgr;


public class OrgWxConfigService {
	private OrgWxConfigService(){
		
	}
	private static class SingletonHolder {
		private static OrgWxConfigService INSTANCE = new OrgWxConfigService();
	}

	public static OrgWxConfigService getInstance() {
		return SingletonHolder.INSTANCE;
	}
	/**
	 * 根据学校编号获得这个学校对应的微信配置
	 * @param orgid
	 * @return
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public OrgWxconfig getWxConfig(String wxorgid){
		String id=RedisId.getInstance().createWxConfig(wxorgid);
		OrgWxconfig orgWxconfig = null;
		String json = null;
		try {
			json = RedisMgr.getInstance().getProp(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (null==json||"".equals(json)) {
			orgWxconfig = new OrgWxconfig().findById(wxorgid);
			if (null != orgWxconfig) {
				json = JSON.toJSONString(orgWxconfig);
				try {
					RedisMgr.getInstance().setProp(id, json);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			orgWxconfig = JSON.parseObject(json, OrgWxconfig.class);
		}
		return orgWxconfig;
	}
}