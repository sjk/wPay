<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="base.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
<title>支付测试</title>
</head>
<body>
	<form action="<%=realUrl%>/auth/xzdAuthorize" method="get">
	<div class="page">
		<div class="weui-cells__title">请求参数</div>
		<div class="weui-cells weui-cells_form">
			<div class="weui-cell">
				<div class="weui-cell__hd"><label class="weui-label">cid</label></div>
				<div class="weui-cell__bd">
					<input class="weui-input" name="cid" type="text" value="xzd">
				</div>
			</div>
		</div>
		<div class="weui-cells weui-cells_form">
			<div class="weui-cell">
				<div class="weui-cell__hd"><label class="weui-label">code</label></div>
				<div class="weui-cell__bd">
					<input class="weui-input" name="code" type="text" value="${code}">
				</div>
			</div>
		</div>
		<div class="weui-cells weui-cells_form">
			<div class="weui-cell">
				<div class="weui-cell__hd"><label class="weui-label">请求json</label></div>
				<div class="weui-cell__bd">
					 <textarea name="param" class="weui-textarea" placeholder="请输入文本" rows="3">${json}</textarea>
				</div>
			</div>
		</div>
		<div class="weui-btn-area">
			<button class="weui-btn weui-btn_primary" id="regSave" type="submit">提交</button>
		</div>
	</div>
	</form>
</body>
</html>