<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>404</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" href="//cdn.bootcss.com/weui/0.4.3/style/weui.min.css">
</head>   
<body>
<div class="weui_msg">
  <div class="weui_icon_area"><i class="weui_icon_warn weui_icon_msg"></i></div>
  <div class="weui_text_area">
    <h2 class="weui_msg_title">抱歉，找不到此页面</h2>
    <p class="weui_msg_desc">
    	Sorry, the site now can not be accessed.
    </p>
    <p class="weui_msg_desc">
    	你请求访问的页面，暂时找不到.
    </p>
  </div>
  <div class="weui_opr_area">
    <p class="weui_btn_area">
     	<a href="javascript:close()" class="weui_btn weui_btn_primary">关闭</a>
    </p>
  </div>
</div>
</body>
<script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<script type="text/javascript">
function close(){
	wx.closeWindow();
}
</script>
</html>