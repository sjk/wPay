<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="base.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>微信支付</title>
	</head>
	<form action="<%=realUrl%>/pay/sendUrl" id="form1">
		<input type="hidden" id="orderId" name="orderId" value="${orderId}">
		<input type="hidden" id="wxorgid" name="wxorgid" value="${wxorgid}">
		<input type="hidden" id="state" name="state">
	</form>
<body>
</body>
<script type="text/javascript">
	function onBridgeReady(){
		var state;
		WeixinJSBridge.invoke(
			'getBrandWCPayRequest', 
			${json},
			function(res){
				// 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。
				if(res.err_msg == "get_brand_wcpay_request:ok" ) {
					state='ok';
				}else if(res.err_msg == "get_brand_wcpay_request:cancel"){
					state='cancel';
				}else if(res.err_msg == "get_brand_wcpay_request:fail"){
					state='fail';
				}
				updateStage(state);
			}
		); 
	}
	if (typeof WeixinJSBridge == "undefined"){
		if( document.addEventListener ){
			document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
		}else if (document.attachEvent){
			document.attachEvent('WeixinJSBridgeReady', onBridgeReady); 
			document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
		}
	}else{
		onBridgeReady();
	}
	
	function updateStage(state){
		document.getElementById("state").value=state;
		document.getElementById("form1").submit();
	}
</script>
</html>