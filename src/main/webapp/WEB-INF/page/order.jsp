<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="base.jsp" %>
<%
	System.out.println("realUrl: "+realUrl);
	String errMsg = request.getParameter("err");
	if(!StringUtils.isEmpty(errMsg)){
		request.removeAttribute("err");
	}
%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=no">
<title>支付测试页面</title>
<link rel="stylesheet"
	href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
<link rel="stylesheet"
	href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
<style type="text/css">
body {
	background-color: #f8f8f8;
}

.d-title {
	text-align: center;
	font-size: 1rem;
	padding-top: 0.5rem;
	padding-bottom: 1rem;
	font-weight: normal;
	color: #666
}
</style>
</head>

<body>
		<div class="page">
			<h1 class="d-title">用户支付</h1>
				<div class="weui-cells__title">付款人信息</div>
				<div class="weui-cells weui-cells_form">
					<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">家长姓名</label></div>
					    <div class="weui-cell__bd">
					      <input class="weui-input" type="text" value="${user.parName}">
					    </div>
					</div>
					<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">学生姓名</label></div>
					    <div class="weui-cell__bd">
					      <input class="weui-input" type="text" value="${user.stuName}">
					    </div>
					</div>
					<div class="weui-cell">
					    <div class="weui-cell__hd"><label class="weui-label">学校</label></div>
					    <div class="weui-cell__bd">
					      <input class="weui-input" type="text" value="${user.schoolName}">
					    </div>
					</div>
				</div>
				
				<div class="weui-cells__title">套餐信息</div>
				<div class="weui-cells weui-cells_form">
					<div class="weui-cell" id="selCharge">
		    			<div class="weui-cell__hd">
		    				<label class="weui-label">套餐名称</label>
		    			</div>
		    			<div class="weui-cell__bd">
		      				<input class="weui-input" id="charge" type="text" readonly="readonly">
		   				</div>
	  				</div>
					<div class="weui-cell" id="selDays">
		    			<div class="weui-cell__hd">
		    				<label class="weui-label">购买数量</label>
		    			</div>
		    			<div class="weui-cell__bd">
		      				<input class="weui-input" id="days" type="text" readonly="readonly">
		   				</div>
	  				</div>
	  			</div>
				
				 <div class="weui-cells__title">验证家长手机号</div>
				 <div class="weui-cells weui-cells_form">
				 	<div class="weui-cell">
			           <div class="weui-cell__hd"><label class="weui-label">手机号</label></div>
				    	<div class="weui-cell__hd">
				      		<input class="weui-input" type="number" id="parMobile" value="${user.mobile}" disabled="disabled">
				    	</div>
			        </div>
			        <div class="weui-cell weui-cell_vcode">
			            <div class="weui-cell__hd">
			                <label class="weui-label">验证码</label>
			            </div>
			            <div class="weui-cell__bd">
			                <input class="weui-input" type="tel" placeholder="请输入验证码" id="mobileVal" disabled="disabled">
			            </div>
			            <div class="weui-cell__ft">
			             	<button id="hsbtn" class="weui-vcode-btn weui-btn_plain-primary" value="" onclick="getMobVal(this)">获取验证码</button>
			            </div>
			        </div>
			    </div>
			    
				<div class="weui-btn-area">
					<button class="weui-btn weui-btn_primary" id="regSave" type="submit">提交</button>
				</div>
		</div>
</body>

<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<script type="text/javascript">
loadDays();
loadCharge();
$(document).ready(function(){
  $("#regSave").click(function(){
	  save();
  });
});

function save(){
	var parMobile = $("#parMobile").val();
	if(parMobile==""){
		$.alert("家长手机号不能为空");
		return false;
	}
	if(parMobile.length!=11){
		$.alert("家长手机号位数不正确");
		return false;
	}
	var mobileVal = $("#mobileVal").val();
	if(mobileVal==""){
		$.alert("验证码不能为空");
		return false;
	}
	var charge = $("#charge").attr("data-values");
	if(charge==""){
		$.alert("套餐名次不能为空");
		return false;
	}
	var days = $("#days").attr("data-values");
	if(days==""){
		$.alert("购买数量不能为空");
		return false;
	}
	$.ajax({
		url:'<%=realUrl%>/auth/isYzm',
		type: 'post',
		dataType: 'json',
		timeout: 100000,
		data:{parMobile:parMobile,mobileVal:mobileVal,chargeCode:charge,month:days},
		error: function(e){alert("error: "+e);},
		success: function(result){
			alert(JSON.stringify(result));
			if(result.errcode==0){
				window.location.href='<%=realUrl%>/auth/forwardOrder';
				return;
			}else{
				$.alert(result.errmsg);
				return;
			}
		}
	})
}
function loadDays(){
	$("#days").select({
		  title: "选择购买数量",
		  items: [
		    {
		      title: "一个月(30天)",
		      value: "1",
		    },
		    {
		      title: "一个季度(90天)",
		      value: "3",
		    },
		    {
		      title: "半年(180天)",
		      value: "6",
		    },
		    {
		      title: "一年(365天)",
		      value: "12",
		    },
		  ]
		});
}

function getMobVal(o){
	var parMobile = $("#parMobile").val();
	if(parMobile==""){
		$.alert("家长手机号不能为空");
		return false;
	}
	if(parMobile.length!=11){
		$.alert("家长手机号位数不正确");
		return false;
	}
	$.ajax({
		url:'<%=realUrl%>/auth/sendYzm',
		type: 'post',
		dataType: 'json',
		timeout: 100000,
		error: function(e){alert("error: "+e);},
		success: function(result){
			if(result.errcode==0){
				$('#mobileVal').removeAttr("disabled");
				$.toast("验证码已发送!");
				time(o);
				return;
			}else{
				$.alert(result.msg);
				return;
			}
		}
	})
}

var wait=120;  
function time(o) {
	 if (wait == 0) {  
         o.removeAttribute("disabled"); 
         o.innerHTML="获取验证码";
         wait = 120;
     } else {  
         o.setAttribute("disabled", "disabled");  
         o.innerHTML=wait + "秒后重新发送";  
         wait--;  
		 setTimeout(function() {  
             time(o)  
         }, 1000)  
	}  
} 


function loadCharge(){
	$("#charge").select(${slist});
}

var err = '<%=errMsg%>';
if(err!=''&&err!='null'){
	alert(err);
}
</script>
</html>